/**
 * 
 */


import java.io.FileReader;
import java.io.IOException;

/**
 * @author miche
 *
 */
public class ParserTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String dirpath = "/home/miche/uni/mcsn/pa/final_13_2/dsl/";
		String sample1 = "DslSample.txt";
		String sample2 = "DslSample2.txt";
		String latin = "LatinSquare3x3.txt";
		FileReader r;
		Parser p;
		ConstraintDsl tree;
		try{
			r = new FileReader(dirpath + "DslSample2.txt");
		} catch (IOException e){
			System.out.println(e);
			return;
		}
		
		p = new Parser(r);
		
		try{
			tree = p.parse();
		} catch(Parser.SyntaxException e) {
			System.out.println(e);
			return;
		}
		
		System.out.println(tree);
	}

}
