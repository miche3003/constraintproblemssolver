import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;


public class SolverGeneratorTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			String dirpath = "/home/miche/uni/mcsn/pa/final_13_2/dsl/";
			String sample1 = "DslSample.txt";
			String sample2 = "DslSample2.txt";
			String sample3 = "Sample3.txt";
			String bad_sample = "BadDslSample.txt";
			String latin = "LatinSquare3x3.txt";
			String binary = "BinarySample.txt";
			String latinComplete = "LatinSquare3x3Complete.txt";
			FileReader r;
			ConstraintSolverGenerator s;
			ConstraintDsl tree;
			try{
				r = new FileReader(dirpath + latinComplete);
			} catch (IOException e){
				System.out.println(e);
				return;
			}
			
			ConstraintSolver.Assignement a = null;
			Date start = new Date();
			int i = 0;
			try{
				s = new ConstraintSolverGenerator(new Parser(r).parse());
				Iterator<ConstraintSolverGenerator.Assignement> it = s.iterator(false);
				while(it.hasNext()){
					System.out.println("SOLUTION: " + it.next());
					i++;
				}
			} catch(Exception e) {
				System.out.println(e);
			}
			Date fin = new Date();
			
			// System.out.println(a);
			System.out.println(String.format("Finished in %s msec, found %d solutions", 
					fin.getTime() - start.getTime(), i));

	}

}
