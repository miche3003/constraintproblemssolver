
public class OverloadTesting {
	
	class Father {
		void foo(){ System.out.println("fooFather"); }
	}
	
	class Son extends Father {
		void foo(){ System.out.println("fooSon"); }
	}
	
	static Father fun1 (Father p){
		if(p instanceof Son){
			Son c = (Son) p;
			fun2(c);
			return c;
		} else {
			fun2(p);
			return p;
		}
	}
	
	static void fun2 (Father p){
		System.out.println("Father");
	}
	
	static void fun2 (Son f){
		System.out.println("Son");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		OverloadTesting t = new OverloadTesting();
		Father p = t.new Son();
		// Father d = fun1(p);
		((Father)p).foo();
	}

}
