// file: ConstraintDsl.java
import java.util.List;
class ConstraintDsl {
  List<Declaration> dl;  List<Constraint> cl;
  ConstraintDsl (List<Declaration> d, List<Constraint> cl) { this.dl = d;  this.cl = cl;  }
  public String toString(){ return dl.toString() + "\n" + cl.toString(); }
}
