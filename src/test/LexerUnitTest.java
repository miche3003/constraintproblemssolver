package test;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Tests for {@link Lexer}.
 *
 * @author michele.carignani@gmail.com (Michele Carignani)
 */
@RunWith(JUnit4.class)
public class LexerUnitTest {

    @Test
    public void thisAlwaysPasses() {
    }

    @Test
//    @Ignore
    public void thisIsIgnored() {
    	Object o1 = new Object(), o2 = new Object();
    	assertEquals(o1, o2);
    }
}
