import java.io.FileReader;
import java.io.IOException;
import java.util.Date;


public class MassiveTesting {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String dirpath = "/home/miche/uni/mcsn/pa/final_13_2/dsl/";
		
		FileReader r;
		ConstraintSolver s;
		ConstraintDsl tree;
		
		String[] test = { 
				"DslSample.txt",
				"DslSample2.txt",
				"Sample3.txt",
				"LatinSquare3x3Complete.txt",
				"BadDslSample.txt"
		};
		int i = 1;
		for(String t : test){
			System.out.println("---------------------\nInit test " + i + "(" + t + ")");
			try{
				r = new FileReader(dirpath + t);
			} catch (IOException e){
				System.out.println(e);
				return;
			}
			
			ConstraintSolver.Assignement a = null;
			Date start = new Date();
			
			try{
				s = new ConstraintSolver(new Parser(r).parse());
				a = s.solve();
			} catch(Exception e) {
				System.out.println(e);
			}
			
			Date fin = new Date();
			
			System.out.println(a);
			System.out.println(String.format("Finished in %s msec.", fin.getTime() - start.getTime()));
			i++;
		}
		
		
		
		
		
		

	}

}
