//file Constraint.java
import java.util.List;
class Constraint {
    List<Implication> il; boolean isComplemented;
    Constraint(List<Implication> il, boolean c){ this.il = il; this.isComplemented = c; }
    public String toString() {return ((isComplemented) ? "!" : "") + il.toString(); }
  }