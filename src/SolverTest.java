import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
/**
 * 
 */

/**
 * @author miche
 *
 */
public class SolverTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String dirpath = "/home/miche/uni/mcsn/pa/final_13_2/dsl/";
		String sample1 = "DslSample.txt";
		String sample2 = "DslSample2.txt";
		String sample3 = "Sample3.txt";
		String bad_sample = "BadDslSample.txt";
		String latin = "LatinSquare3x3.txt";
		String latinComplete = "LatinSquare3x3Complete.txt";
		FileReader r;
		ConstraintSolver s;
		ConstraintDsl tree;
		try{
			r = new FileReader(dirpath + latinComplete);
		} catch (IOException e){
			System.out.println(e);
			return;
		}
		
		ConstraintSolver.Assignement a = null;
		Date start = new Date();
		
		try{
			s = new ConstraintSolver(new Parser(r).parse());
			a = s.solve();
		} catch(Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		Date fin = new Date();
		
		System.out.println(a);
		if(a != null)
		System.out.println(String.format("Finished in %s msec.", fin.getTime() - start.getTime()));

	}

}
