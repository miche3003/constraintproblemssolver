// file Implication.java
class Implication {
    Ide condition, consequence; boolean isComplemented;
    Implication(Ide v1, Ide v2, boolean compl){ this.condition = v1; 
     this.consequence = v2; this.isComplemented = compl; }
    public String toString() {return String.format("( %s, %s)", condition, consequence);}
  }