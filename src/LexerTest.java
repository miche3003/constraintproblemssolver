import java.io.FileReader;
import java.io.IOException;


public class LexerTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String path = "/home/miche/uni/mcsn/pa/final_13_2/dsl/DslSample.txt";
		FileReader r;
		Lexer l;
		Lexer.Terminal t;
		
		try{
			r = new FileReader(path);
		} catch (IOException e){
			System.out.println(e);
			return;
		}

		l = new Lexer(r);
		t = l.nextToken();
		while(t != Lexer.Terminal.EOF){
			System.out.println(t);
			t = l.nextToken();
		}
		
	}

}
