// file: Lexer.java
import java.io.IOException; 
import java.io.Reader;
import java.io.StreamTokenizer;
class Lexer {
  StreamTokenizer input;  int token;
  Lexer(Reader r) 
  	{ input = new StreamTokenizer(r); input.ordinaryChars('a', 'Z');
  	 char[] ords = {'(', ')', '{', '!', '=', ','};  for(char c : ords){input.ordinaryChar(c);}}
  int currentLine () {  return input.lineno(); }
  String getWord () {  return input.sval;  }
  Terminal nextToken() 
   {  try 
    	{token = input.nextToken();
		for(Terminal symbol: Terminal.values())
			{ if (((char) token) == (char) symbol.value){ return symbol; } }
    	switch(token)
        	{ case StreamTokenizer.TT_EOF: return Terminal.EOF;
        	case StreamTokenizer.TT_WORD: return Terminal.STRING;
        	default: return Terminal.EOF; }} catch(IOException e){return Terminal.EOF; }}
  enum Terminal
	{ INVALID, NO_TOKEN, STRING, EOF, LIST_OPEN('{'), LIST_CLOSE ('}'), 	COMMA(','),
	COMPLEMENT('!'), IMPL_OPEN  ('('), IMPL_CLOSE  (')'), EQUAL('=');
	char value;
	private Terminal(char c ){this.value = c; 	}
	private Terminal() { this.value = 0; }}
}