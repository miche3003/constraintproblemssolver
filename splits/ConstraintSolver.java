//file ConstraintSolver.java
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ConstraintSolver {
  ConstraintDsl syntaxtree; Assignement fallimento = null;  
  String[] variables; LinkedList<Domain> domains; ArrayList<Con> constraints;
  ConstraintSolver (ConstraintDsl syntax_tree) 
    {syntaxtree = syntax_tree; }
  class Con {
    String pre; String post; boolean compl;
    Con(String pr, String po, boolean compl){ pre = pr; post = po; }
    public String toString()
      {return 
          (this.compl) ? String.format("!(%s, %s)", pre, post)
          : String.format("!(%s, %s)", pre, post); }
  }
  class Assignement {
    Domain[] doms;
    String explanation;
    Assignement (int varsNum) 
      { this.doms = new Domain[varsNum]; this.explanation = new String();}
    boolean complete()
      { for(int i = 0; i < doms.length; i++) if(!(doms[i].explicitSet))return false;
      return true; }
    /** Applies the simplest strategy: first in the list not yet assigned */
    int choose_not_assigned()
      { for(int i = 0; i < doms.length; i++){ if(!doms[i].explicitSet) return i; }
      return -1; }
    void set(String val){ doms[where(val)].setTo(val); }
    public Assignement clone()
      { Assignement a2 = new Assignement(this.doms.length);
      for(int i = 0; i < doms.length; i++){
        a2.doms[i] = (Domain) this.doms[i].clone();
      }
      a2.explanation = new String(this.explanation);
      return a2; }
    int where (String v)
      { for(int i = 0; i < doms.length; i++) if(doms[i].contains(v)) return i; 
      return -1; }
    public String toString()
      { String r = "";
      for(int i = 0; i < doms.length; i++){
        r = r + doms[i]; if(i != doms.length -1) r  = r + ", ";
      }
      return String.format("{ %s }", r); }
    int getVarIdx(String valData){
      for(int i = 0; i < doms.length; i++) if(doms[i].contains(valData)){return i;} 
      return -1;
    }
  }
  class Domain {
    LinkedList<String> elements; boolean explicitSet = false;
    int idx;
    Domain (int idx) { elements = new LinkedList<String>(); this.idx = idx;}
    Domain (LinkedList<String> vl, int i) { elements = vl; idx = i;}
    Domain (LinkedList<String> vl, boolean set, int i) { elements = vl; explicitSet = set; idx = i;}
    void add(String v){ elements.add(v); }
    void setTo(String v){elements.clear();elements.add(v); explicitSet = true; }
    boolean isSingleton(){ return elements.size() == 1; }
    boolean isEmpty(){  return this.elements.isEmpty(); }
    boolean contains(String valData){   return this.elements.contains(valData); }
    void remove(String post){ elements.remove(post); }
    @SuppressWarnings("unchecked")
    public Domain clone(){
      /* Note: list elements do not need to be copied */
      return new Domain((LinkedList<String>) elements.clone(), this.explicitSet, this.idx);
    }
    /* Simplest strategy: order is the same of immission */
    String[] ordina_valori_dominio()
      { return elements.toArray( new String[elements.size()] ); }
    public String toString()
      {return String.format("{%s=%s}", variables[this.idx], 
          (this.isSingleton()) ? elements.get(0) : elements); } 
  }
  Assignement solve ()
    { try { MakeDisjointDomains(); MakeConstraintList(); }
    catch(Exception e){ System.out.println(e); return null; }
    return Backtracking(initial_assignement()); }
  /** returns soluzione o fallimento */
  Assignement Backtracking(Assignement a)
    { if(a.complete()){ return a; }
    int varIdx = a.choose_not_assigned();
    for(String v: a.doms[varIdx].ordina_valori_dominio()){
      Assignement a2 = check_up(this.constraints, a, v, varIdx);
      if (a2 != fallimento)
        { a2.set(v);
        Assignement forward = Backtracking(a2);
        if(forward != null){ return forward; }  }
    }
    return fallimento; }
  Assignement check_up(ArrayList<Con> cons, Assignement a, String v, int varIdx){
    int j;Assignement a2 = a.clone();
    for(Con i: cons){
      if(i.pre.equals(v)){
        j = a2.where(i.post);
        if(i.compl){
          a2.doms[j].remove(i.post);
          if(a2.doms[j].isEmpty()){ return fallimento; }
        } else {
          if(j < 0 ) {return fallimento;}
          a2.doms[j].setTo(i.post);
        }
      }
    }
    return a2;
  }
  Assignement initial_assignement()
    {  int i = 0; Assignement a = new Assignement(this.domains.size());
    for(Domain d : this.domains) { a.doms[i] = d; i++; }
    return a; }
  boolean MakeDisjointDomains() throws Exception
    { HashSet<String> set = new HashSet<String>();
    this.variables = new String[syntaxtree.dl.size()];
    this.domains = new LinkedList<Domain>();
    String var; Domain dom; int i = 0;
    
    for(Declaration d: syntaxtree.dl.toArray(new Declaration[1])){
      dom = new Domain(i); 
      var = d.i.data; 
      this.domains.add(dom);
      for(Ide v : d.vs.toArray(new Ide[1])){
        if(!set.add(v.data)){
          throw new Exception("Illegal domain variable repeatition: " + v.data);
        } else {
          dom.add(v.data);
        }
      }
      this.variables[i] = var; i++;
     }
    return true; }
  void MakeConstraintList()
    {this.constraints = new ArrayList<Con>();
    for(Constraint c : this.syntaxtree.cl)
      for(Implication i: c.il)
        this.constraints.add(
            new Con(i.condition.data, i.consequence.data, i.isComplemented));}
}
