Assignement Backtracking(Assignement a, LinkedList<Assignement> stack)
	{ Assignement result = null;
	if(a.complete()){ return a; }
	int varIdx = a.choose_not_assigned();
	for(String v: a.doms[varIdx].ordina_valori_dominio()){
		Assignement a2 = check_up(this.constraints, a, v, varIdx);
		if (a2 != fallimento){
			a2.set(v);
			if(result == null){ 
				Assignement forward = Backtracking(a2, stack);
				if(forward != null){ result = forward; }
			}else if(!stack.contains(a2)){ stack.push(a2); }
		}
	}
	return result;}
class Solutions implements Iterator<Assignement> {
	LinkedList<Assignement> stack; Assignement currentSolution;
	String[] variables; LinkedList<Domain> domains; ArrayList<Con> constraints;
	Solutions(String[] vars, LinkedList<Domain> doms, ArrayList<Con> cons) {
		variables = vars; domains = doms; constraints = cons; 
		this.stack = new LinkedList<Assignement>();
		stack.push(initial_assignement());
	}
	public boolean hasNext()
		{ while(!stack.isEmpty()){
			Assignement res = Backtracking(stack.pop(), stack);
			if(res != null){ currentSolution = res; return true; }
		}
		return false; }
	public Assignement next() { return currentSolution; }
	public void remove() { return; }
}
public Iterator<Assignement> iterator()
	{try {
		MakeDisjointDomains(); MakeConstraintList();
	} catch(Exception e){ System.out.println(e); return null; }
	return new Solutions(this.variables, this.domains, this.constraints);}	