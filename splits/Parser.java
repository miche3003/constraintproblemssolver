// file: Parser.java
import java.io.Reader; import java.util.LinkedList; import java.util.List;
class Parser {
  Lexer.Terminal token; Lexer input;
  Parser (Reader r){ input = new Lexer(r); nextToken(); }
  public ConstraintDsl parse() throws SyntaxException {
    List<Declaration> d = declist();  List<Constraint> c = constlist(); 
    return new ConstraintDsl(d, c);
  }
  List<Declaration> declist () throws SyntaxException {
     List<Declaration> decs = new LinkedList<Declaration>(); decs.add(declaration());
     while (token != Lexer.Terminal.LIST_OPEN  && token != Lexer.Terminal.COMPLEMENT)
     {  decs.add(declaration()); }
     return decs;
  }
  Declaration declaration () throws SyntaxException{
    Ide i = ide();  expect(Lexer.Terminal.EQUAL);
    expect(Lexer.Terminal.LIST_OPEN); List<Ide> vals = idelist();
    expect(Lexer.Terminal.LIST_CLOSE); return new Declaration(i, vals);
  }
  List<Constraint> constlist () throws SyntaxException {
  Constraint con = constraint();
  if(con == null){ return new LinkedList<Constraint>(); }
  List<Constraint> constrs = (new LinkedList<Constraint>()); constrs.add(con);
    while(token != Lexer.Terminal.EOF){  constrs.add(constraint());  }
    return constrs;
  }
  Constraint constraint () throws SyntaxException {
    boolean isComplemented = false;
    if(token == Lexer.Terminal.COMPLEMENT)
     { expect(Lexer.Terminal.COMPLEMENT); isComplemented = true;  }
    expect(Lexer.Terminal.LIST_OPEN);
    if (token == Lexer.Terminal.LIST_CLOSE){ nextToken(); return null; }
    List<Implication> impls = impllist(isComplemented);
    Constraint con = new Constraint(impls, isComplemented); 
    expect(Lexer.Terminal.LIST_CLOSE);  return con;
  }
  List<Ide> idelist () throws SyntaxException {
    List<Ide> vals = new LinkedList<Ide>(); vals.add(ide());
    while(token == Lexer.Terminal.COMMA){ nextToken(); vals.add(ide()); }
    return vals;
  }
  Ide ide () throws SyntaxException {
  if(token != Lexer.Terminal.STRING)
    { throw new SyntaxException("Expected identifier but found " + token); }
    
    String i = input.getWord(); nextToken(); return new Ide(i);
  }
  List<Implication> impllist (boolean compl) throws SyntaxException {
    List<Implication> impls = new LinkedList<Implication>();
    impls.add(implication(compl));
    while(token == Lexer.Terminal.COMMA){ nextToken(); impls.add(implication(compl));}
    return impls;
  }
  Implication implication (boolean isComplemented) throws SyntaxException {
    expect(Lexer.Terminal.IMPL_OPEN); Ide v1 = ide();
    expect(Lexer.Terminal.COMMA); Ide v2 = ide();
    expect(Lexer.Terminal.IMPL_CLOSE); return new Implication(v1, v2, isComplemented);
  }
  void expect(Lexer.Terminal s) throws SyntaxException {
    if (s != token){ throw new SyntaxException(s, token, input.currentLine()); }
    nextToken();
  }
  void nextToken() { token = input.nextToken();  }
  class SyntaxException extends Exception {
  private static final long serialVersionUID = 3818423772849466341L;
  Lexer.Terminal ex, rec; String s = null; int l;
    SyntaxException(Lexer.Terminal ex, Lexer.Terminal rec, int l) {this.ex = ex; this.rec = rec;}
    SyntaxException(String s){ this.s = s; }
    public String toString(){ 
      if(this.s != null) return s;
      return "Expected " + this.ex + " but received " + this.rec + " in line " + l;
    }
  }
}
